*** Settings ***

Resource        base.robot

Test Setup      Nova sessão
Test Teardown   Encerra sessão

*** Test Cases ***

CPF Inválido
    
    Go To                           ${url}
    Sleep                           3
    Login With                      02899233113       18-01-1994        01-01-2010
    Should Contain Login Alert      CPF inválido
    Mouse Out                       id:content

*** Keywords ***
Login With
    [Arguments]                     ${cpf}        ${dtNasc}     ${dthab}

    Input Text                      css:input[name=cpf]                             ${cpf} 
    Input Text                      css:input[name=dataNascimento]                  ${dtNasc}
    Input Text                      css:input[name=dataPrimeiraHabilitacao]         ${dthab}
    Click Button                    class:btn-primary.btn-block.btn

Should Contain Login Alert
    [Arguments]                     ${expected_message}

    ${message}=                     Get WebElement                 id:content
    Should Contain                  ${message.text}                ${expected_message} 