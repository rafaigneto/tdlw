Crie uma rotina de teste End2End, que acesso o site,

https://www.detran.mg.gov.br/habilitacao/prontuario/consultar-pontuacao-na-cnh/

logue com os seguintes dados :

cpf: 02899233113
data de nascimento: 18-01-1994
data primeira habilitação: 01-01-2010
e verifique se o retorno será ("NUMERO DO CPF INEXISTENTE")

Criar uma descrição do caso de testes usando BDD.
Criar uma pipeline de testes utilizando gitlab-ci.

	BDD:

		Funcionalidade: Login
			Todos queremos realizar a consulta da pontuação da CNH

			Cenário: Retorno CPF Inexistente
				Dado que estou na página do Detran de consultar pontuação na CNH
				Quando eu fornecer os dados com CPF: 02899233113 
				E data de nascimento: 18-01-1994
				E data primeira habilitação: 01-01-2010
				Então será exibido uma mensagem de "Número do CPF Inexistente"
