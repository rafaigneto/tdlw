*** Settings ***
Library                     SeleniumLibrary
Library                     webdriver_manager
Library                     lxml

*** Variables ***
${url}                      https://www.detran.mg.gov.br/habilitacao/prontuario/consultar-pontuacao-na-cnh/

${RF_SENSITIVE_VARIABLE}    ${EMPTY}
${GRID_URL}                 http://selenium:4444/wd/hub
${BROWSER}                  chrome

*** Keywords ***
Nova Sessão
    Open Browser            ${url}      chrome
Encerra sessão
    Sleep                   3
    Capture Page Screenshot    
    Close Browser